part of 'app_config_cubit.dart';

enum ThemeEnum { dark, light }

@freezed
class AppConfigState with _$AppConfigState {
  const AppConfigState._();

  const factory AppConfigState({
    required ThemeMode themeMode,
    required Locale locale,
  }) = _AppConfigState;

  factory AppConfigState.initial() => const AppConfigState(
        themeMode: ThemeMode.system,
        locale: Locale('vi'),
      );

  bool get isDarkMode => themeMode == ThemeMode.dark;
}
