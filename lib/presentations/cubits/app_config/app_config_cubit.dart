// Flutter imports:

// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'app_config_cubit.freezed.dart';
part 'app_config_state.dart';

class AppConfigCubit extends Cubit<AppConfigState> {
  AppConfigCubit() : super(AppConfigState.initial());

  void themeTogged(isDark) => emit(
      state.copyWith(themeMode: isDark ? ThemeMode.dark : ThemeMode.light));

  /// Subtract 1 from the current state.
  void localeChanged(locale) => emit(state.copyWith(locale: locale));
}
