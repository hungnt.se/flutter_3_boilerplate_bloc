part of 'auth_bloc.dart';

@freezed
class AuthEvent with _$AuthEvent {
  const factory AuthEvent.tokenChanged({
    String? token,
  }) = _TokenChanged;

  const factory AuthEvent.logoutRequested() = _LogoutRequested;
}
