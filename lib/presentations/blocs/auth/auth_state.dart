part of 'auth_bloc.dart';

@freezed
class AuthState with _$AuthState {
  const factory AuthState.unknown() = _Unknown;

  const factory AuthState.authenticated({required String token}) = _Authenticated;

  const factory AuthState.unauthenticated() = _UnAuthenticated;
}
