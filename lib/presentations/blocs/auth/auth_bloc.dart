// Dart imports:
import 'dart:async';

// Package imports:
import 'package:domain/domain.dart' show SignOutUseCase, TokenChangeUseCase;
import 'package:flutter_bloc/flutter_bloc.dart';
// Project imports:
import 'package:freezed_annotation/freezed_annotation.dart';

part 'auth_bloc.freezed.dart';
part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  late final StreamSubscription<String?> _tokenSubscription;
  final TokenChangeUseCase _tokenChangeUserCase;
  final SignOutUseCase _signOutUseCase;

  AuthBloc({
    required TokenChangeUseCase tokenChangeUseCase,
    required SignOutUseCase signOutUseCase,
  })  : _tokenChangeUserCase = tokenChangeUseCase,
        _signOutUseCase = signOutUseCase,
        super(const AuthState.unknown()) {
    on<_TokenChanged>(_onTokenChanged);
    on<_LogoutRequested>(_onLogoutRequested);
    _tokenSubscription = _tokenChangeUserCase()
        .listen((token) => add(_TokenChanged(token: token)));
  }

  void _onTokenChanged(_TokenChanged event, Emitter<AuthState> emit) {
    emit(event.token != null
        ? AuthState.authenticated(token: event.token!)
        : const AuthState.unauthenticated());
  }

  void _onLogoutRequested(_LogoutRequested event, Emitter<AuthState> emit) {
    _signOutUseCase();
  }

  @override
  Future<void> close() {
    _tokenSubscription.cancel();
    return super.close();
  }
}
