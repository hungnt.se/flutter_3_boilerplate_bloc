import 'package:auto_route/auto_route.dart';

import 'dashboard_page.dart';

const dashboardTab = AutoRoute(
  path: 'dashboard',
  name: 'DashboardTab',
  page: DashboardPage,
);
