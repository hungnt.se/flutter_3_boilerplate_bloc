// Flutter imports:
import 'package:flutter/material.dart';
import 'package:flutter_3_boilerplate_bloc/core/route/route.dart';

class MainAppPage extends StatelessWidget {
  const MainAppPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AutoTabsRouter.tabBar(
      routes: const [
        DashboardTab(),
        HomeTab(),
        SettingTab(),
      ],
      builder: (context, child, _) {
        return Scaffold(
          appBar: AppBar(
            title: Text(context.topRoute.name),
          ),
          body: child,
          bottomNavigationBar: buildBottomNav(context, context.tabsRouter),
        );
      },
    );
  }

  Widget buildBottomNav(BuildContext context, TabsRouter tabsRouter) {
    final hideBottomNav = tabsRouter.topMatch.meta['hideBottomNav'] == true;
    return hideBottomNav
        ? const SizedBox.shrink()
        : BottomNavigationBar(
            currentIndex: tabsRouter.activeIndex,
            onTap: tabsRouter.setActiveIndex,
            items: const [
              BottomNavigationBarItem(
                icon: Icon(Icons.source),
                label: 'Dashboard',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.source),
                label: 'Home',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.person),
                label: 'Settings',
              ),
            ],
          );
  }
}
