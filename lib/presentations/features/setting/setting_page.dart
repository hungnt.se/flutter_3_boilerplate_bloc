// Flutter imports:
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// Project imports:
import 'package:flutter_3_boilerplate_bloc/core/gen/l10n.dart';
import 'package:flutter_3_boilerplate_bloc/presentations/blocs/auth/auth_bloc.dart';
import 'package:flutter_3_boilerplate_bloc/presentations/cubits/cubits.dart';
// Package imports:
import 'package:flutter_bloc/flutter_bloc.dart';

// Project imports:

class SettingPage extends StatelessWidget {
  const SettingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WillPopScope(
        onWillPop: () async => false,
        child: Center(
          child: Column(
            children: [
              Text(S.of(context).simpleText),
              BlocBuilder<AppConfigCubit, AppConfigState>(
                builder: (context, state) {
                  return CupertinoSwitch(
                    value: state.isDarkMode,
                    onChanged: (isDark) =>
                        context.read<AppConfigCubit>().themeTogged(isDark),
                  );
                },
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    onPressed: () => context
                        .read<AppConfigCubit>()
                        .localeChanged(const Locale('en')),
                    child: const Text('En'),
                  ),
                  const SizedBox(width: 16),
                  ElevatedButton(
                    onPressed: () => context
                        .read<AppConfigCubit>()
                        .localeChanged(const Locale('vi')),
                    child: const Text('Vi'),
                  ),
                ],
              ),
              ElevatedButton(
                onPressed: () => context
                    .read<AuthBloc>()
                    .add(const AuthEvent.logoutRequested()),
                child: const Text('Logout'),
              )
            ],
          ),
        ),
      ),
    );
  }
}
