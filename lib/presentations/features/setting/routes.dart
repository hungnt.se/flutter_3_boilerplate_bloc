import 'package:auto_route/auto_route.dart';
import 'package:flutter_3_boilerplate_bloc/presentations/features/setting/setting_page.dart';


const settingsTab = AutoRoute(
  path: 'settings',
  name: 'SettingTab',
  page: SettingPage,
);
