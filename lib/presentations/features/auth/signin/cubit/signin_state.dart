part of 'signin_cubit.dart';

@freezed
abstract class SignInState with _$SignInState {
  const SignInState._();
  const factory SignInState({
    required Email email,
    required Password password,
    required FormzStatus status,
    String? errorMessage,
  }) = _SignInState;

  factory SignInState.empty() => const SignInState(
      email: Email.dirty(), password: Password.dirty(), status: FormzStatus.pure);
}
