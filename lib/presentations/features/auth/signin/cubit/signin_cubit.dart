import 'package:domain/domain.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:form_inputs/form_inputs.dart';
import 'package:formz/formz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'signin_cubit.freezed.dart';
part 'signin_state.dart';

class SignInCubit extends Cubit<SignInState> {
  final SignInUseCase _signInUseCase;
  SignInCubit(this._signInUseCase)
      : super(SignInState.empty().copyWith(
          email: const Email.dirty('eve.holt@reqres.in'),
          password: const Password.dirty('cityslicka'),
          status: FormzStatus.valid
        ));

  void emailChanged(String value) {
    final email = Email.dirty(value);
    emit(
      state.copyWith(
        email: email,
        status: Formz.validate([email, state.password]),
      ),
    );
  }

  void passwordChanged(String value) {
    final password = Password.dirty(value);
    emit(
      state.copyWith(
        password: password,
        status: Formz.validate([state.email, password]),
      ),
    );
  }

  Future<void> logInWithCredentials() async {
    // if (!state.status.isValidated) return;
    emit(state.copyWith(status: FormzStatus.submissionInProgress));
    final res = await _signInUseCase(
        params: AuthRequestParams(
            email: state.email.value, password: state.password.value));

    res.fold(
      (failure) => emit(
        state.copyWith(
          errorMessage: failure.message,
          status: FormzStatus.submissionFailure,
        ),
      ),
      (user) => emit(state.copyWith(status: FormzStatus.submissionSuccess)),
    );
  }
}
