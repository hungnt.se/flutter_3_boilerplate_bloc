// Flutter imports:
import 'package:domain/domain.dart' show SignInUseCase;
import 'package:flutter/material.dart';
import 'package:flutter_3_boilerplate_bloc/core/di/init_di.dart';

// Package imports:
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';

// Project imports:
import 'cubit/signin_cubit.dart';
import 'signin_form.dart';

class SignInPage extends StatelessWidget {
  const SignInPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: BlocProvider(
            create: (_) => SignInCubit(getIt<SignInUseCase>()),
            child: BlocListener<SignInCubit, SignInState>(
              listener: (context, state) {
                if (state.status.isSubmissionFailure) {
                  ScaffoldMessenger.of(context)
                    ..hideCurrentSnackBar()
                    ..showSnackBar(
                      SnackBar(
                        content: Text(
                            state.errorMessage ?? 'Authentication Failure'),
                      ),
                    );
                }
              },
              child: const SignInForm(),
            ),
          ),
        ),
      ),
    );
  }
}
