// Flutter imports:
import 'package:flutter/material.dart';
import 'package:flutter_3_boilerplate_bloc/core/route/route.dart';

class TodosPage extends StatelessWidget {
  const TodosPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            const Text('Todos pages'),
            ElevatedButton(
              onPressed: () => context.router.push(const TodoCreateRoute()),
              child: const Text('Go create todo'),
            )
          ],
        ),
      ),
    );
  }
}
