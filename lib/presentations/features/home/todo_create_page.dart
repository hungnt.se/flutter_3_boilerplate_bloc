// Flutter imports:
import 'package:flutter/material.dart';
import 'package:flutter_3_boilerplate_bloc/core/route/route.dart';

class TodoCreatePage extends StatelessWidget {
  const TodoCreatePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            const Text('Todo create pages'),
            ElevatedButton(
              onPressed: () => context.router.pop(),
              child: const Text('Go back'),
            ),
          ],
        ),
      ),
    );
  }
}
