import 'package:auto_route/auto_route.dart';

import 'todo_create_page.dart';
import 'todos_page.dart';

const homeTab = AutoRoute(
  path: 'home',
  name: 'HomeTab',
  page: EmptyRouterPage,
  children: [
    AutoRoute(path: 'todos', page: TodosPage, initial: true),
    AutoRoute(path: 'create', page: TodoCreatePage),
  ],
);
