import 'package:flutter/material.dart';
import 'package:flutter_3_boilerplate_bloc/core/route/route.dart';
import 'package:flutter_3_boilerplate_bloc/presentations/blocs/auth/auth_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class StartPage extends StatelessWidget {
  const StartPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<AuthBloc, AuthState>(
        builder: (context, state) {
          return WillPopScope(
            onWillPop: () async => false,
            child: AutoRouter.declarative(
              routes: (context) => [
                state.map(
                  unknown: (_) => const SplashRoute(),
                  unauthenticated: (_) => const SignInRoute(),
                  authenticated: (_) => const AppRoute(),
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
