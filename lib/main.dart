// Dart imports:
import 'dart:async';
import 'dart:developer';

// Flutter imports:
import 'package:domain/domain.dart' show SignOutUseCase, TokenChangeUseCase;
import 'package:flutter/material.dart';
// Package imports:
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:utils/utils.dart' show AppBlocObserver;

// Project imports:
import 'core/di/init_di.dart';
import 'core/gen/l10n.dart';
import 'core/route/route.dart';
import 'core/theme/app_theme.dart';
import 'presentations/blocs/auth/auth_bloc.dart';
import 'presentations/cubits/cubits.dart' show AppConfigCubit, AppConfigState;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  FlutterError.onError = (details) {
    log(details.exceptionAsString(), stackTrace: details.stack);
  };

  await initializeDependencies();

  runZonedGuarded(
    () async {
      await BlocOverrides.runZoned(
        () async => runApp(const MyApp()),
        blocObserver: AppBlocObserver(),
      );
    },
    (error, stackTrace) => log(error.toString(), stackTrace: stackTrace),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final appRouter = getIt<AppRouter>();
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => AppConfigCubit(),
        ),
        BlocProvider(
          create: (context) => AuthBloc(
            tokenChangeUseCase: getIt<TokenChangeUseCase>(),
            signOutUseCase: getIt<SignOutUseCase>()
          ),
        ),
      ],
      child: BlocBuilder<AppConfigCubit, AppConfigState>(
        builder: (context, state) {
          return MaterialApp.router(
            debugShowCheckedModeBanner: false,
            builder: (context, widget) => ResponsiveWrapper.builder(
              ClampingScrollWrapper.builder(context, widget!),
              breakpoints: const [
                ResponsiveBreakpoint.resize(450, name: MOBILE),
                ResponsiveBreakpoint.autoScale(800, name: TABLET),
                ResponsiveBreakpoint.autoScale(1000, name: TABLET),
                ResponsiveBreakpoint.resize(1200, name: DESKTOP),
                ResponsiveBreakpoint.autoScale(2460, name: "4K"),
              ],
            ),
            supportedLocales: S.delegate.supportedLocales,
            localizationsDelegates: const [
              S.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
            ],
            locale: state.locale,
            themeMode: state.themeMode,
            theme: lightTheme,
            darkTheme: darkTheme,
            routerDelegate: AutoRouterDelegate(
              appRouter,
              navigatorObservers: () => [RouteObserver()],
            ),
            routeInformationParser: AppRouter().defaultRouteParser(),
          );
          
        },
      ),
    );
  }
}
