// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

import 'package:auto_route/auto_route.dart' as _i6;
import 'package:flutter/material.dart' as _i10;

import '../../presentations/features/app_page.dart' as _i4;
import '../../presentations/features/auth/auth_page.dart' as _i3;
import '../../presentations/features/dashboard/dashboard_page.dart' as _i5;
import '../../presentations/features/home/todo_create_page.dart' as _i9;
import '../../presentations/features/home/todos_page.dart' as _i8;
import '../../presentations/features/setting/setting_page.dart' as _i7;
import '../../presentations/features/splash_page.dart' as _i2;
import '../../presentations/features/start_page.dart' as _i1;

class AppRouter extends _i6.RootStackRouter {
  AppRouter([_i10.GlobalKey<_i10.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i6.PageFactory> pagesMap = {
    StartRoute.name: (routeData) {
      return _i6.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i1.StartPage());
    },
    SplashRoute.name: (routeData) {
      return _i6.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i2.SplashPage());
    },
    SignInRoute.name: (routeData) {
      return _i6.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i3.SignInPage());
    },
    AppRoute.name: (routeData) {
      return _i6.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i4.MainAppPage());
    },
    DashboardTab.name: (routeData) {
      return _i6.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i5.DashboardPage());
    },
    HomeTab.name: (routeData) {
      return _i6.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i6.EmptyRouterPage());
    },
    SettingTab.name: (routeData) {
      return _i6.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i7.SettingPage());
    },
    TodosRoute.name: (routeData) {
      return _i6.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i8.TodosPage());
    },
    TodoCreateRoute.name: (routeData) {
      return _i6.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i9.TodoCreatePage());
    }
  };

  @override
  List<_i6.RouteConfig> get routes => [
        _i6.RouteConfig(StartRoute.name, path: '/', children: [
          _i6.RouteConfig('#redirect',
              path: '',
              parent: StartRoute.name,
              redirectTo: 'splash',
              fullMatch: true),
          _i6.RouteConfig(SplashRoute.name,
              path: 'splash', parent: StartRoute.name),
          _i6.RouteConfig(SignInRoute.name,
              path: 'signin', parent: StartRoute.name),
          _i6.RouteConfig(AppRoute.name,
              path: 'app',
              parent: StartRoute.name,
              children: [
                _i6.RouteConfig(DashboardTab.name,
                    path: 'dashboard', parent: AppRoute.name),
                _i6.RouteConfig(HomeTab.name,
                    path: 'home',
                    parent: AppRoute.name,
                    children: [
                      _i6.RouteConfig('#redirect',
                          path: '',
                          parent: HomeTab.name,
                          redirectTo: 'todos',
                          fullMatch: true),
                      _i6.RouteConfig(TodosRoute.name,
                          path: 'todos', parent: HomeTab.name),
                      _i6.RouteConfig(TodoCreateRoute.name,
                          path: 'create', parent: HomeTab.name)
                    ]),
                _i6.RouteConfig(SettingTab.name,
                    path: 'settings', parent: AppRoute.name)
              ])
        ]),
        _i6.RouteConfig('*#redirect',
            path: '*', redirectTo: '/', fullMatch: true)
      ];
}

/// generated route for
/// [_i1.StartPage]
class StartRoute extends _i6.PageRouteInfo<void> {
  const StartRoute({List<_i6.PageRouteInfo>? children})
      : super(StartRoute.name, path: '/', initialChildren: children);

  static const String name = 'StartRoute';
}

/// generated route for
/// [_i2.SplashPage]
class SplashRoute extends _i6.PageRouteInfo<void> {
  const SplashRoute() : super(SplashRoute.name, path: 'splash');

  static const String name = 'SplashRoute';
}

/// generated route for
/// [_i3.SignInPage]
class SignInRoute extends _i6.PageRouteInfo<void> {
  const SignInRoute() : super(SignInRoute.name, path: 'signin');

  static const String name = 'SignInRoute';
}

/// generated route for
/// [_i4.MainAppPage]
class AppRoute extends _i6.PageRouteInfo<void> {
  const AppRoute({List<_i6.PageRouteInfo>? children})
      : super(AppRoute.name, path: 'app', initialChildren: children);

  static const String name = 'AppRoute';
}

/// generated route for
/// [_i5.DashboardPage]
class DashboardTab extends _i6.PageRouteInfo<void> {
  const DashboardTab() : super(DashboardTab.name, path: 'dashboard');

  static const String name = 'DashboardTab';
}

/// generated route for
/// [_i6.EmptyRouterPage]
class HomeTab extends _i6.PageRouteInfo<void> {
  const HomeTab({List<_i6.PageRouteInfo>? children})
      : super(HomeTab.name, path: 'home', initialChildren: children);

  static const String name = 'HomeTab';
}

/// generated route for
/// [_i7.SettingPage]
class SettingTab extends _i6.PageRouteInfo<void> {
  const SettingTab() : super(SettingTab.name, path: 'settings');

  static const String name = 'SettingTab';
}

/// generated route for
/// [_i8.TodosPage]
class TodosRoute extends _i6.PageRouteInfo<void> {
  const TodosRoute() : super(TodosRoute.name, path: 'todos');

  static const String name = 'TodosRoute';
}

/// generated route for
/// [_i9.TodoCreatePage]
class TodoCreateRoute extends _i6.PageRouteInfo<void> {
  const TodoCreateRoute() : super(TodoCreateRoute.name, path: 'create');

  static const String name = 'TodoCreateRoute';
}
