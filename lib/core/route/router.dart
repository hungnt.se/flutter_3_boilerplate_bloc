// Package imports:
import 'package:auto_route/auto_route.dart';
import 'package:flutter_3_boilerplate_bloc/presentations/features/start_page.dart';
import 'package:flutter_3_boilerplate_bloc/presentations/features/auth/auth_page.dart';
import 'package:flutter_3_boilerplate_bloc/presentations/features/dashboard/routes.dart';
import 'package:flutter_3_boilerplate_bloc/presentations/features/home/routes.dart';
import 'package:flutter_3_boilerplate_bloc/presentations/features/app_page.dart';
import 'package:flutter_3_boilerplate_bloc/presentations/features/splash_page.dart';

import '../../presentations/features/setting/routes.dart';

// Project imports:

@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: <AutoRoute>[
    AutoRoute(
      path: '/',
      page: StartPage,
      children: [
        AutoRoute(path: 'splash', page: SplashPage, initial: true),
        AutoRoute(path: 'signin', page: SignInPage),
        AutoRoute(
          path: 'app',
          name: 'AppRoute',
          page: MainAppPage,
          children: [
            dashboardTab,
            homeTab,
            settingsTab,
          ],
        ),
      ],
    ),
    RedirectRoute(path: '*', redirectTo: '/'),
  ],
)
class $AppRouter {}
