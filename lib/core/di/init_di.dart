// Package imports:
import 'package:data/data.dart' show setupDataDependencies;
import 'package:dio/dio.dart';
import 'package:domain/domain.dart';
import 'package:get_it/get_it.dart';

// Project imports:
import 'package:flutter_3_boilerplate_bloc/core/route/route.dart';
import '../consts/http_const.dart';

import 'package:shared/shared.dart'
    show
        SharedPreferenceHelper,
        setupDioDependencies,
        setupSharedPreferenceDependencies;

final getIt = GetIt.instance;

Future<void> initializeDependencies() async {
  await setupSharedPreferenceDependencies(getIt);
  getIt.registerSingleton<AppRouter>(AppRouter());

  await setupDioDependencies(
    getIt,
    getIt<SharedPreferenceHelper>(),
    BaseOptions(
      baseUrl: HttpConst.baseUrl,
      connectTimeout: HttpConst.connectionTimeout,
      receiveTimeout: HttpConst.receiveTimeout,
    ),
  );

  await setupDataDependencies(
    getIt: getIt,
    dio: getIt<Dio>(),
    sharedPreferenceHelper: getIt<SharedPreferenceHelper>(),
  );

  await setupUseCaseDependencies(getIt);
}
