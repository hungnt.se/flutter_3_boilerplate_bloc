import 'dart:async';
import 'dart:convert';

import 'package:rx_shared_preferences/rx_shared_preferences.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared/shared.dart' show Crypto, LocalDataSourceException;

import 'shared_preference_const.dart';

class SharedPreferenceHelper {
  // shared pref instance
  final RxSharedPreferences _rxPrefs;
  final Crypto _crypto;

  // constructor
  SharedPreferenceHelper(this._rxPrefs, this._crypto);

  // General Methods: ----------------------------------------------------------
  @override
  Future<String?> get authToken async {
    return _rxPrefs.getString(SharedPreferenceConst.authToken);
  }

  @override
  Future<void> saveAuthToken(String authToken) async {
    return _rxPrefs
        .write<String>(SharedPreferenceConst.authToken, authToken,
            (u) => u == null ? null : jsonEncode(u))
        .onError<Object>(
          (e, s) => throw LocalDataSourceException(
              'Cannot save user and token', e, s),
        );
  }

  @override
  Stream<String?> get tokenChanges => _rxPrefs
      .observe<String>(SharedPreferenceConst.authToken,
          (u) => u == null ? null : jsonEncode(u))
      .onErrorReturnWith((e, s) =>
          throw LocalDataSourceException('Cannot read user and token', e, s));

  @override
  Future<void> removeAuthToken() async {
    return _rxPrefs.remove(SharedPreferenceConst.authToken).onError<Object>((e,
            s) =>
        throw LocalDataSourceException('Cannot delete user and token', e, s));
  }

  // Theme:------------------------------------------------------

  @override
  Future<void> changeBrightnessToDark(bool value) {
    return _rxPrefs.setBool(SharedPreferenceConst.isDarkMode, value);
  }

  // Language:---------------------------------------------------

  @override
  Future<String?> get currentLanguage async {
    return _rxPrefs.getString(SharedPreferenceConst.currentLanguage);
  }

  @override
  Future<void> changeLanguage(String language) {
    return _rxPrefs.setString(SharedPreferenceConst.currentLanguage, language);
  }
}
