import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:get_it/get_it.dart';
import 'package:rx_shared_preferences/rx_shared_preferences.dart';

import 'module/module.dart';
import 'sharedpref/shared_preference_helper.dart';
import 'utils/ciphers.dart';

Future<void> setupSharedPreferenceDependencies(GetIt getIt) async {
  // async singletons:----------------------------------------------------------
  getIt.registerSingletonAsync<SharedPreferences>(
      () => LocalModule.provideSharedPreferences());

  // singletons:----------------------------------------------------------------
  getIt.registerSingleton<Crypto>(MethodChannelCryptoImpl());

  getIt.registerSingleton<RxSharedPreferences>(
    RxSharedPreferences(
      await getIt.getAsync<SharedPreferences>(),
      kReleaseMode ? null : const RxSharedPreferencesDefaultLogger(),
    ),
  );

  getIt.registerSingleton<SharedPreferenceHelper>(
      SharedPreferenceHelper(getIt<RxSharedPreferences>(), getIt<Crypto>()));
}

Future<void> setupDioDependencies(GetIt getIt,
    SharedPreferenceHelper sharedPreferenceHelper, BaseOptions options) async {
  getIt.registerSingleton<Dio>(
    NetworkModule.provideDio(sharedPreferenceHelper, options),
  );
}
