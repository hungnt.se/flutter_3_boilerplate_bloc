// Package imports:
import 'package:dartz/dartz.dart';
import 'package:shared/shared.dart' show Failure;

// Project imports:
import '../entities/user.dart';

abstract class IAuthRepository {
  // API methods
  Future<Either<Failure, String>> signIn(String email, String password);

  Stream<String?> get tokenChanges;

  User? getCurrentUser();

  Future<void> signOut();
}
