import 'package:get_it/get_it.dart';

import 'repositories/repositories.dart';
import 'usecases/usecases.dart';

Future<void> setupUseCaseDependencies(GetIt getIt) async {
  // UseCases - Auth
  getIt.registerSingleton<SignInUseCase>(
      SignInUseCase(getIt<IAuthRepository>()));
  getIt.registerSingleton<SignOutUseCase>(
      SignOutUseCase(getIt<IAuthRepository>()));
  getIt.registerSingleton<TokenChangeUseCase>(
      TokenChangeUseCase(getIt<IAuthRepository>()));
  getIt.registerSingleton<GetUserUseCase>(
      GetUserUseCase(getIt<IAuthRepository>()));
}
