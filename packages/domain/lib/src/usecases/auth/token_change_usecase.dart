// Project imports:
import '../../repositories/auth_repository.dart';
import '../usecase.dart';

class TokenChangeUseCase implements UseCase<Stream<String?>, void> {
  final IAuthRepository repository;

  TokenChangeUseCase(this.repository);

  @override
  call({void params}) {
    return repository.tokenChanges;
  }
}
