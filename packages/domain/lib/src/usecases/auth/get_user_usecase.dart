import '../../entities/user.dart';
import '../../repositories/auth_repository.dart';
import '../usecase.dart';

class GetUserUseCase implements UseCase<User?, void> {
  final IAuthRepository repository;

  GetUserUseCase(this.repository);

  @override
  call({void params}) {
    return repository.getCurrentUser();
  }
}
