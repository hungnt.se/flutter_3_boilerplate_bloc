// Project imports:
import '../../repositories/auth_repository.dart';
import '../usecase.dart';

class SignOutUseCase implements UseCase<Future<void>, void> {
  final IAuthRepository repository;

  SignOutUseCase(this.repository);

  @override
  call({void params}) async {
    await repository.signOut();
  }
}
