// Package imports:
import 'package:dartz/dartz.dart';
// Project imports:
import 'package:shared/shared.dart' show Failure;

import '../../repositories/auth_repository.dart';
import '../usecase.dart';

class AuthRequestParams {
  final String email;
  final String password;

  const AuthRequestParams({
    required this.email,
    required this.password,
  });
}

class SignInUseCase
    implements UseCase<Future<Either<Failure, String>>, AuthRequestParams> {
  final IAuthRepository repository;

  SignInUseCase(this.repository);

  @override
  call({required params}) async {
    return repository.signIn(params.email, params.password);
  }
}
