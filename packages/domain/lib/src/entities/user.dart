class User {
  final int id;
  final String email;
  final String username;
  final String name;
  final String phone;

  User({
    required this.id,
    required this.email,
    required this.username,
    required this.name,
    required this.phone,
  });
}
