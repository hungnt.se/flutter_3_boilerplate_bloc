// Package imports:
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logger/logger.dart';

Logger logger(Type type) => Logger(
      printer: _CustomerPrinter(type.toString()),
      level: Level.verbose,
    );

class AppBlocObserver extends BlocObserver {
  final log = logger(AppBlocObserver);

  @override
  void onChange(BlocBase bloc, Change change) {
    super.onChange(bloc, change);

    log.i('onChange[${bloc.runtimeType}] value: $change');
  }

  @override
  void onError(BlocBase bloc, Object error, StackTrace stackTrace) {
    log.e('onError(${bloc.runtimeType}, $error, $stackTrace)');
    super.onError(bloc, error, stackTrace);
  }
}

class _CustomerPrinter extends LogPrinter {
  final String className;

  _CustomerPrinter(this.className);
  @override
  List<String> log(LogEvent event) {
    final color = PrettyPrinter.levelColors[event.level];
    final emoji = PrettyPrinter.levelEmojis[event.level];
    final message = event.message;

    return [color!('$emoji: $message')];
  }
}
