import 'package:dartz/dartz.dart';
import 'package:domain/domain.dart' show IAuthRepository, User;
import 'package:shared/shared.dart' show Failure, SharedPreferenceHelper;

import '../datasources/remote/auth/auth_api.dart';
import 'repository_utils.dart';

class AuthRepositoryImpl extends RepositoryUtils implements IAuthRepository {
  final AuthApi _authApi;
  final SharedPreferenceHelper _prefs;

  AuthRepositoryImpl(this._authApi, this._prefs);

  @override
  Stream<String?> get tokenChanges => _prefs.tokenChanges;

  @override
  Future<Either<Failure, String>> signIn(email, password) async {
    return await convertToEither<String>(
      () async {
        final res = await _authApi
            .login(RequestLoginDto(email: email, password: password));
        final token = res.data.token.toString();
        _prefs.saveAuthToken(token);
        return token;
      },
    );
  }

  @override
  User? getCurrentUser() {
    return null;
  }

  @override
  Future<void> signOut() async {
    _prefs.removeAuthToken();
  }
}
