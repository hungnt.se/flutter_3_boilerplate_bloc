import 'package:dio/dio.dart';
import 'package:domain/domain.dart';
import 'package:get_it/get_it.dart';
import 'package:shared/shared.dart';

import 'datasources/remote/auth/auth_api.dart';
import 'repositories/repositories.dart';

Future<void> setupDataDependencies({
  required GetIt getIt,
  required Dio dio,
  required SharedPreferenceHelper sharedPreferenceHelper,
}) async {
  getIt.registerSingleton<AuthApi>(AuthApi(dio));

  getIt.registerSingleton<IAuthRepository>(
      AuthRepositoryImpl(getIt<AuthApi>(), sharedPreferenceHelper));
}
